﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Zoom : MonoBehaviour
{
    public GameObject player;
    float sensitivity = 17f;
    float minFov = 35;
    float maxFov = 100;
    void Update()
    {
        float fov = Camera.main.fieldOfView;
        fov += Input.GetAxis("Mouse ScrollWheel") * -sensitivity;
        fov = Mathf.Clamp(fov, minFov, maxFov);
        Camera.main.fieldOfView = fov;
    }
    
   
}