﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Camera : MonoBehaviour
{
    public List<GameObject> players2;
    private Camera _cameras;
    public float dampTime = 0.15f;
    public float smoothTime = 2f;
    public float zoomvalue;
    private Vector3 velocity = Vector3.zero;
    public GameObject player;
    float sensitivity = 17f;
    float minFov = 35;
    float maxFov = 100;


    void Start()
    {
        _cameras = GetComponent<Camera>();
    }

    void LateUpdate()
    {
        float fov = Camera.main.fieldOfView;
        fov += Input.GetAxis("Mouse ScrollWheel") * -sensitivity;
        fov = Mathf.Clamp(fov, minFov, maxFov);
        Camera.main.fieldOfView = fov;
        if (players2[GameManager.Player2] != null)
        {
            Vector3 point = _cameras.WorldToViewportPoint(players2[GameManager.Player2].transform.position);
            Vector3 delta = players2[GameManager.Player2].transform.position - _cameras.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
            Vector3 destination = transform.position + delta;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
        }
    }
}

