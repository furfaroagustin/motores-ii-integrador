﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMedico : MonoBehaviour
{
    public GameObject Planet;
   
    public LayerMask capaPiso;
    public float magnitudSalto;
    public SphereCollider col;





    public float speed = 4;
    
    float gravity = 100;
    bool OnGround = false;


    float distanceToGround;
    Vector3 Groundnormal;

    public int playerNumber;

    private Rigidbody rb;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
       


    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.actualPlayer == playerNumber)
        {
            Movimiento();
        }
        void Movimiento()
        {
            float x = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
            float z = Input.GetAxis("Vertical") * Time.deltaTime * speed;
            if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
            {
                rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
                GestorAudio.instancia.ReproducirSonido("salto");
            }


            transform.Translate(x, 0, z);

            //Local Rotation

            if (Input.GetKey(KeyCode.D))
            {

                transform.Rotate(0, 150 * Time.deltaTime, 0);
            }
            if (Input.GetKey(KeyCode.A))
            {

                transform.Rotate(0, -150 * Time.deltaTime, 0);
            }






            //GroundControl

            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(transform.position, -transform.up, out hit, 10))
            {

                distanceToGround = hit.distance;
                Groundnormal = hit.normal;

                if (distanceToGround <= 0.2f)
                {
                    OnGround = true;
                }
                else
                {
                    OnGround = false;
                }


            }


            //GRAVITY and ROTATION

            Vector3 gravDirection = (transform.position - Planet.transform.position).normalized;

            if (OnGround == false)
            {
                rb.AddForce(gravDirection * -gravity);

            }

            //

            Quaternion toRotation = Quaternion.FromToRotation(transform.up, Groundnormal) * transform.rotation;
            transform.rotation = toRotation;



        }

    }
      
    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }




}


