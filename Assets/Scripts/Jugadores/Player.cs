﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public GameObject Planet;
    public float speed = 4;
    public float JumpHeight = 1.2f;
    float gravity = 100;
    bool OnGround = false;
    float distanceToGround;
    Vector3 Groundnormal;
    private Rigidbody rb;
    public int playerNumber;
    public GameObject bala;//bullet
    public Transform puntoSpawn;//spawnPoint
    public float fuerzaDisparo = 1500;//shotforce
    public float shotRate = 0.5f;//radio de disparo
    private float shotRateTime = 0;//tiempo entre disparo 
    public Transform puntoBombaSpawn;//spawnPoint
    public float fuerzaDisparoBomba = 1500;//shotforce
    public float radioDisparo = 0.5f;//radio de disparo
    private float tiempoEntreDisparo = 0;//tiempo entre disparo
    public GameObject bomba;//bullet
    public GameObject metralleta;
    public Transform metralletaSpawn;
    public float metralletaShotRate = 0.5f;
    public float metralletaShotRateTime = 0;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public SphereCollider col;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        GestorAudio.instancia.ReproducirSonido("principal");
    }
    // Update is called once per frame
    void Update()
    {
        if (GameManager.actualPlayer == playerNumber)
        { 
            Movimiento();
        
        }
           
    
    }
    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }
  
    void Movimiento()
    {
        //MOVEMENT
        float x = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        float z = Input.GetAxis("Vertical") * Time.deltaTime * speed;
        transform.Translate(x, 0, z);
        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            GestorAudio.instancia.ReproducirSonido("salto");
        }
        if (Input.GetKey("c"))
        {
            if (Time.time > shotRateTime)
            {
                GestorAudio.instancia.ReproducirSonido("laser");
                GameObject nuevaBala;//NewBullet
                nuevaBala = Instantiate(bala, puntoSpawn.position, puntoSpawn.rotation);
                nuevaBala.GetComponent<Rigidbody>().AddForce(puntoSpawn.forward * fuerzaDisparo);
                shotRateTime = Time.time + shotRate;
                Destroy(nuevaBala, 2);

            }

        }
        if (Input.GetKey("f"))
        {
            if (Time.time > tiempoEntreDisparo)
            {

                GameObject disparoBomba;//NewBullet
                disparoBomba = Instantiate(bomba, puntoBombaSpawn.position, puntoBombaSpawn.rotation);
                disparoBomba.GetComponent<Rigidbody>().AddForce(puntoBombaSpawn.forward * fuerzaDisparo);
                tiempoEntreDisparo = Time.time + radioDisparo;
                Destroy(disparoBomba, 10);
                GestorAudio.instancia.ReproducirSonido("boom");

            }

        }
        if (Input.GetKey("g"))
        {
            if (Time.time > metralletaShotRateTime)
            {

                GameObject nuevaMetralleta;//NewBullet
                nuevaMetralleta = Instantiate(metralleta, metralletaSpawn.position, metralletaSpawn.rotation);
                nuevaMetralleta.GetComponent<Rigidbody>().AddForce(metralletaSpawn.forward * fuerzaDisparo);
                metralletaShotRateTime = Time.time + metralletaShotRate;
                Destroy(nuevaMetralleta, 10);
                GestorAudio.instancia.ReproducirSonido("metralla");

            }

        }



        //Local Rotation
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0, 150 * Time.deltaTime, 0);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0, -150 * Time.deltaTime, 0);
        }
        //GroundControl
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(transform.position, -transform.up, out hit, 10))
        {
            distanceToGround = hit.distance;
            Groundnormal = hit.normal;
            if (distanceToGround <= 0.2f)
            {
                OnGround = true;
            }
            else
            {
                OnGround = false;
            }
        }
        //GRAVITY and ROTATION
        Vector3 gravDirection = (transform.position - Planet.transform.position).normalized;

        if (OnGround == false)
        {
            rb.AddForce(gravDirection * -gravity);
        }
        //
        Quaternion toRotation = Quaternion.FromToRotation(transform.up, Groundnormal) * transform.rotation;
        transform.rotation = toRotation;
    }






}
