﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement2 : MonoBehaviour
{
	public float speed = 2f;
	public LayerMask capaPiso;
	public float magnitudSalto;
	public SphereCollider col;
	private Rigidbody rb;
	public int playerNumber;
	public GameObject bala;//bullet
	public Transform puntoSpawn;//spawnPoint
	public float fuerzaDisparo = 1500;//shotforce
	public float shotRate = 0.5f;//radio de disparo
	private float shotRateTime = 0;//tiempo entre disparo 
	public Transform puntoBombaSpawn;//spawnPoint
	public float fuerzaDisparoBomba = 1500;//shotforce
	public float radioDisparo = 0.5f;//radio de disparo
	private float tiempoEntreDisparo = 0;//tiempo entre disparo
	public GameObject bomba;//bullet
	public GameObject metralleta;
	public Transform metralletaSpawn;
	public float metralletaShotRate = 0.5f;
	public float metralletaShotRateTime = 0;
	Vector3 move;
	private void Start()
	{
		rb = GetComponent<Rigidbody>();

		col = GetComponent<SphereCollider>();

	}
	private void Update()
	{
		if (GameManager.Player2 == playerNumber)
		{
			Movimiento();
		}



	}

	void Movimiento()
	{
		move.x = Input.GetAxis("Horizontal2");
		if (Input.GetKeyDown(KeyCode.UpArrow) && EstaEnPiso())
		{
			GestorAudio.instancia.ReproducirSonido("salto");
			rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
		}
		if (Input.GetKey("j"))
		{
			if (Time.time > shotRateTime)
			{
				//GestorDeAudio.instancia.ReproducirSonido("laser");
				GameObject nuevaBala;//NewBullet
				nuevaBala = Instantiate(bala, puntoSpawn.position, puntoSpawn.rotation);
				nuevaBala.GetComponent<Rigidbody>().AddForce(puntoSpawn.forward * fuerzaDisparo);
				shotRateTime = Time.time + shotRate;
				Destroy(nuevaBala, 2);

			}

		}
		if (Input.GetKey("l"))
		{
			if (Time.time > tiempoEntreDisparo)
			{
				//GestorDeAudio.instancia.ReproducirSonido("laser");
				GameObject disparoBomba;//NewBullet
				disparoBomba = Instantiate(bomba, puntoBombaSpawn.position, puntoBombaSpawn.rotation);
				disparoBomba.GetComponent<Rigidbody>().AddForce(puntoBombaSpawn.forward * fuerzaDisparo);
				tiempoEntreDisparo = Time.time + radioDisparo;
				Destroy(disparoBomba, 10);

			}

		}
		if (Input.GetKey("k"))
		{
			if (Time.time > metralletaShotRateTime)
			{

				GameObject nuevaMetralleta;//NewBullet
				nuevaMetralleta = Instantiate(metralleta, metralletaSpawn.position, metralletaSpawn.rotation);
				nuevaMetralleta.GetComponent<Rigidbody>().AddForce(metralletaSpawn.forward * fuerzaDisparo);
				metralletaShotRateTime = Time.time + metralletaShotRate;
				Destroy(nuevaMetralleta, 10);
				GestorAudio.instancia.ReproducirSonido("metralla");

			}

		}


	}

	void FixedUpdate()
	{
		transform.Translate(move * Time.fixedDeltaTime * speed);
	}

	private bool EstaEnPiso()
	{
		return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
	}


}
