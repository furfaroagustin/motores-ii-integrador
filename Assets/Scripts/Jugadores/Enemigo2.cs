﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemigo2 : MonoBehaviour
{
   
    public float velocidad = 5.0f;
 
    private Rigidbody enemigo;
    private void Start()
    {
        enemigo = GetComponent<Rigidbody>();

    }
    private void Update()
    {
        enemigo.AddForce(Vector3.forward * -velocidad);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);

        }
        if (other.gameObject.CompareTag("Barrera"))
        {
            Destroy(this.gameObject);

        }
        if (other.gameObject.CompareTag("ActivarA"))
        {
            Destroy(gameObject);
        }

    }





}
