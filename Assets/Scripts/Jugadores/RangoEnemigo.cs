﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangoEnemigo : MonoBehaviour
{
    public Animator ani;
    public MovementEnemigo enemigo;
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            ani.SetBool("walk",false);
            ani.SetBool("run",false);
            ani.SetBool("attack",true);
            enemigo.atacando = true;
            GetComponent<BoxCollider>().enabled = false;
        }
    }
}
