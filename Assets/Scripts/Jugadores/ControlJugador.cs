﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ControlJugador : MonoBehaviour
{
    public float speed = 5;
    private Rigidbody rb;
    public static bool lastKeyUp;
    public int playerNumber;
    private void Start()
    {
        rb = GetComponent<Rigidbody>(); 
    }
    public virtual void FixedUpdate()
    {
        if (GameManager.actualPlayer == playerNumber)
        {
            Movement();
        }
    }
    private void Movement()
    {
        float inputX = Input.GetAxis("Horizontal");
        float inputZ = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(speed * inputX, speed * inputZ);
        rb.velocity = movement;
        if (Input.GetKey(KeyCode.A))
        {
            lastKeyUp = true;
        }
        else
        if (Input.GetKey(KeyCode.D))
        {
            lastKeyUp = true;
        }
    }
}