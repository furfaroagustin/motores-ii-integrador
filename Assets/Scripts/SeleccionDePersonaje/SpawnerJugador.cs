﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerJugador : MonoBehaviour
{
    
    void Start()
    {
        Instantiate(PlayerStorage.playerPrefab, this.transform.position, this.transform.rotation);
        Destroy(this.gameObject);
    }

 
    void Update()
    {
        
    }
}
//instanciar el prefab del player storage y se instancia con rotacion y posicion del spawner