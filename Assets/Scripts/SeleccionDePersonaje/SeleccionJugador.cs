﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//añadimos una funcion publica  que recibe el indice y este lo utilizamos para activar el cuadro de seleccion que toca 
//y almacenar el prefb del jugador que vamos a spawnear en el storage 
public class SeleccionJugador : MonoBehaviour
{
    public Image[] selectionBoxes;
    public GameObject[] prefabs;
    void Start()
    {
        foreach (var img in this.selectionBoxes)
        {
            img.gameObject.SetActive(false);
        }
        this.Select(0);
        
    }
    public void Select(int index)
    {
        foreach(var img in this.selectionBoxes) 
        {
            img.gameObject.SetActive(false);
        
        }
        this.selectionBoxes[index].gameObject.SetActive(true);
        PlayerStorage.playerPrefab = this.prefabs[index];
    }

   
    void Update()
    {
        
    }
}
