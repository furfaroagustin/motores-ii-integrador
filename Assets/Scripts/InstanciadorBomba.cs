﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciadorBomba : MonoBehaviour
{
    private float rangeGeneration = 9.0f;
    public GameObject prefabKey;
    private void Start()
    {
        float posXGeneration = Random.Range(-rangeGeneration, rangeGeneration);
        float posZGeneration = Random.Range(-rangeGeneration, rangeGeneration);
        Vector3 posAleatoria = new Vector3(posXGeneration, 6f, posZGeneration);
        Instantiate(prefabKey, posAleatoria, prefabKey.transform.rotation);
    }
}
