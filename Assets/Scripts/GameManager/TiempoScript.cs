﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TiempoScript : MonoBehaviour
{
    public Text contador;
    public Text fin;
    private float tiempo=10f;
    void Start()
    {
        contador.text = " " + tiempo;
        fin.enabled = false;
    }

    
    void Update()
    {
        tiempo -= Time.deltaTime;
        contador.text = "" + tiempo.ToString("0");
        if (tiempo <= 0) 
        {
            contador.text = "0";
            fin.enabled = true;
        
        }
    }
}
