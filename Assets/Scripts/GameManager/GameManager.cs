﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
   
    public static int actualPlayer = 0;
 
    public List<ControlTarget> targets;

    public List<Player> players;
    
    public static int Player2 = 0;
    public List<Player2>Playerr2;

    void Start()
    {
        Physics.gravity = new Vector3(0, -30, 0);
  
        
    }

    void Update()
    {
        GetInput2();
        GetInput();
        

    }

    private void GetInput2()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (Player2 <= 0)
            {
                Player2 = 6;

            }
            else
            {
                Player2--;

            }
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            if (Player2 >= 6)
            {
                Player2 = 0;

            }
            else
            {
                Player2++;

            }
        }
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (actualPlayer <= 0)
            {
                actualPlayer = 7;
                
            }
            else
            {
                actualPlayer--;
               
            }
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (actualPlayer >= 7)
            {
                actualPlayer = 0;
                
            }
            else
            {
                actualPlayer++;
                
            }
        }
        
    }
}
