﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bandera : MonoBehaviour
{
	public int maxHealth = 0;
	public int currentHealth;

	public HealtBar healthBar;

	// Start is called before the first frame update
	void Start()
	{
		currentHealth = maxHealth;
		healthBar.SetMaxHealth(maxHealth);
	}

	// Update is called once per frame
	void Update()
	{


	}
	private void OnCollisionEnter(Collision colision)
	{
	
		if (colision.gameObject.CompareTag("Player"))
		{
			GiveLife(10);
		}
		

	}

	void TakeDamage(int damage)
	{
		currentHealth -= damage;

		healthBar.setHealth(currentHealth);
	}
	void GiveLife(int damage)
	{
		currentHealth += damage;

		healthBar.setHealth(currentHealth);
	}


	public void Destruccion()
	{
		Destroy(this.gameObject);


		//Persistencia.instancia.GuardarDataPersistencia();

	}
}
