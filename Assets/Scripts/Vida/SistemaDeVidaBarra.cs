﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SistemaDeVidaBarra : MonoBehaviour
{
    public float maxVidaa = 1f;
    public float actualVidaa;
    
    
   // public Text textVidaPiedras;


    private void Start()
    {
        actualVidaa = maxVidaa;
    }
    public void Update()
    {
        if (actualVidaa > maxVidaa)
        {
            actualVidaa = maxVidaa;


        }
        if (actualVidaa <= 0)
        {
            Muerte();


        }

        //textVidaPiedras.text = actualVidaa.ToString();



    }

    public void QuitarVida(float daño)
    {
        actualVidaa -= daño;
        

    }
    public void DarVida(float vida)
    {
        actualVidaa += vida;

    }
    public void Muerte()
    {
        Destroy(this.gameObject);
        

    }




   
}
