﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class VidaEnemigo : MonoBehaviour
{
    public float maxVidaE = 100.0f;
    public float actualVidaE;
    public bool inmortal = false;
    public float tiempoInmortal = 1.0f;
    public Text textVidaE;


    private void Start()
    {
        actualVidaE = maxVidaE;
    }
    public void Update()
    {
        if (actualVidaE > maxVidaE)
        {
            actualVidaE = maxVidaE;


        }
        if (actualVidaE <= 0)
        {
            Muerte();


        }

        //textVidaE.text = actualVidaE.ToString();



    }

    public void QuitarVida(float daño)
    {
        if (inmortal) return;
        actualVidaE -= daño;
        StartCoroutine(TiempoInmortal());

    }
    public void DarVida(float vida)
    {
        actualVidaE += vida;

    }
    public void Muerte()
    {
        Destroy(this.gameObject);
        //SceneManager.LoadScene(4);

    }
    IEnumerator TiempoInmortal()
    {
        inmortal = true;
        yield return new WaitForSeconds(tiempoInmortal);
        inmortal = false;

    }



}
