﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class SistemaVida : MonoBehaviour
{
    public float maxVida = 100.0f;
    public float actualVida;
    public bool inmortal = false;
    public float tiempoInmortal = 1.0f;
    //public Text textVida;
    [SerializeField] int vidas;
    [SerializeField] Slider sliderVidas;
    //public Image barraDeVida;


    private void Start()
    {
        actualVida = maxVida;
    }
    public void Update()
    {
        //barraDeVida.fillAmount = maxVida / actualVida;
        if (actualVida> maxVida) 
        {
            actualVida = maxVida;
           
        
        }
        if (actualVida<=0)
        {
            Muerte();

            
        }
        
       // textVida.text = actualVida.ToString();

        

    }

    public void QuitarVida(float daño) 
    {
        if (inmortal) return;
        actualVida -= daño;
        StartCoroutine(TiempoInmortal());
    
    }
    public void DarVida(float vida) 
    {
        actualVida += vida;
    
    }
    public void Muerte() 
    {
        Destroy(this.gameObject);
        SceneManager.LoadScene("GameOver");
        
        //Persistencia.instancia.GuardarDataPersistencia();

    }
    IEnumerator TiempoInmortal() 
    {
      inmortal = true;
      yield return new WaitForSeconds(tiempoInmortal);
      inmortal = false;
    
    }

    private void OnCollisionEnter(Collision colision)
    {
        if (colision.gameObject.CompareTag("BalaAmarilla")) 
        {
            vidas--;
            sliderVidas.value = vidas;
            Destroy(this.gameObject);

        }
    }



}
