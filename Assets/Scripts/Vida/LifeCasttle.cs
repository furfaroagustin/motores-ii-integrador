﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LifeCasttle : MonoBehaviour
{
	public int maxHealth = 100;
	public int currentHealth;

	public HealtBar healthBar;

	
	void Start()
	{
		currentHealth = maxHealth;
		healthBar.SetMaxHealth(maxHealth);
	}

	// Update is called once per frame
	void Update()
	{


	}
	private void OnCollisionEnter(Collision colision)
	{
		if (colision.gameObject.CompareTag("BalaAmarilla"))
		{
			TakeDamage(10);
		}
		if (colision.gameObject.CompareTag("Bala"))
	    {
			TakeDamage(10);
		}
		if (colision.gameObject.CompareTag("Teledirigido"))
		{
			TakeDamage(10);
		}
		if (colision.gameObject.CompareTag("Bomba"))
		{
			TakeDamage(30);
		}

		if (currentHealth <= 0)
		{
			Destruccion();
			SceneManager.LoadScene("GameOver");
			GestorAudio.instancia.ReproducirSonido("muerte");


		}
		if (colision.gameObject.CompareTag("Medico"))
		{
			GiveLife(5);
		}
		if (colision.gameObject.CompareTag("Aldeano"))
		{
			GiveLife(5);
		}


	}

	void TakeDamage(int damage)
	{
		currentHealth -= damage;

		healthBar.setHealth(currentHealth);
	}
	void GiveLife(int damage)
	{
		currentHealth += damage;

		healthBar.setHealth(currentHealth);
	}


	public void Destruccion()
	{
		Destroy(this.gameObject);


		//Persistencia.instancia.GuardarDataPersistencia();

	}

}
