﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuboVida : MonoBehaviour
{

    public float vida = 20;
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<SistemaVida>() != null)
        {
            other.gameObject.GetComponent<SistemaVida>().DarVida(vida);
        }
        if(other.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
        }
      



    }

}

