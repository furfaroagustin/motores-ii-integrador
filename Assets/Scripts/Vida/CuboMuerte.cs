﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuboMuerte : MonoBehaviour
{
    public float daño = 20;
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<SistemaVida>()!=null)
        {
            other.gameObject.GetComponent<SistemaVida>().QuitarVida(daño);
        }
        if (other.gameObject.GetComponent<SistemaDeVidaBarra>() != null)
        {
            other.gameObject.GetComponent<SistemaDeVidaBarra>().QuitarVida(daño);
        }
      



    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<VidaEnemigo>() != null)
        {
            collision.gameObject.GetComponent<VidaEnemigo>().QuitarVida(daño);
        }
    }

}
