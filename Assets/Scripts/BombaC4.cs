﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BombaC4 : MonoBehaviour
{
    public GameObject efectoExplosion;
    public Text contadorText;
    
    public int tiempoToText;
    public float trowForce = 40f;
    public float timeToExplode = 10f;
    bool tocoSuelo;
    public GameObject bomba;
    public float power = 10.0f;
    public float radius = 5.0f;
    public float upForce = 1.0f;
    void Start()
    {
        tocoSuelo = false;
    }
    private void FixedUpdate()
    {
        if (bomba == enabled)
        {
            Invoke("Detonate", 20);

        }
    }
    void Detonate()
    {
        Vector3 explosionPosition = bomba.transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPosition, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(power, explosionPosition, radius, upForce, ForceMode.Impulse);

            }
            if (efectoExplosion != null)// Si nuestra explosion esta diferente de null esta activada
            {
                Instantiate(efectoExplosion, transform.position, transform.rotation);

                Destroy(gameObject);
                GestorAudio.instancia.ReproducirSonido("boom");
                if (gameObject.CompareTag("Player"))
                {
                    Destroy(gameObject);
                }

            }

        }
    }
    void Update()
    {

        if (tocoSuelo == true)
        {
            timeToExplode -= Time.deltaTime;
        }
        if (timeToExplode <= 0)
        {



            Destroy(this.gameObject);
        }


    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            tocoSuelo = true;
        }

    }
}
