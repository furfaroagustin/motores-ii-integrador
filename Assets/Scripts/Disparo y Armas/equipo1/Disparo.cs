﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    public GameObject bala;//bullet
    public Transform puntoSpawn;//spawnPoint


    public float fuerzaDisparo = 1500;//shotforce
    public float shotRate = 0.5f;//radio de disparo

    private float shotRateTime = 0;//tiempo entre disparo 
    private void Start()
    {


    }
    void Update()
    {
        if (Input.GetKey("t"))
        {
            if (Time.time > shotRateTime)
            {
                //GestorDeAudio.instancia.ReproducirSonido("laser");
                GameObject nuevaBala;//NewBullet
                nuevaBala = Instantiate(bala, puntoSpawn.position, puntoSpawn.rotation);
                nuevaBala.GetComponent<Rigidbody>().AddForce(puntoSpawn.forward * fuerzaDisparo);
                shotRateTime = Time.time + shotRate;
                Destroy(nuevaBala, 1);
            }




        }
    }


}