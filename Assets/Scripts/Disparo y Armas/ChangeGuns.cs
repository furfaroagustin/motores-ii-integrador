﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeGuns : MonoBehaviour
{
    public GameObject armaPrincipal;
    public GameObject armaSecundaria;
    public GameObject cuchillo;
    public GameObject metralleta;
    void Start()
    {

    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            armaPrincipal.SetActive(true);
            armaSecundaria.SetActive(false);
            cuchillo.SetActive(false);
            metralleta.SetActive(false);

        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            armaPrincipal.SetActive(false);
            armaSecundaria.SetActive(true);
            cuchillo.SetActive(false);
            metralleta.SetActive(false);

        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            armaPrincipal.SetActive(false);
            armaSecundaria.SetActive(false);
            cuchillo.SetActive(true);
            metralleta.SetActive(false);

        }
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            armaPrincipal.SetActive(false);
            armaSecundaria.SetActive(false);
            cuchillo.SetActive(false);
            metralleta.SetActive(true);

        }
    }
}
