﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Misil : MonoBehaviour
{
    private Rigidbody rbEnemigo;
    private GameObject jugador;
    private float speed=5f;
    
    void Start()
    {
        rbEnemigo = GetComponent<Rigidbody>();
        jugador = GameObject.Find("Torre 2");
    }

   
    void Update()
    {
        rbEnemigo.AddForce(jugador.transform.position - transform.position);
        transform.LookAt(jugador.transform);
    }
}
