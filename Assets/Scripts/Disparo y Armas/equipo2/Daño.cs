﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Daño : MonoBehaviour
{
	public int maxHealth = 100;
	public int currentHealth;

	public HealtBar healthBar;

	// Start is called before the first frame update
	void Start()
	{
		currentHealth = maxHealth;
		healthBar.SetMaxHealth(maxHealth);
	}

	// Update is called once per frame
	void Update()
	{


	}
	private void OnCollisionEnter(Collision colision)
	{
		if (colision.gameObject.CompareTag("Bala"))
		{
			TakeDamage(2);
		}
		if (colision.gameObject.CompareTag("Tierra"))
		{
			TakeDamage(20);
		}
		if (currentHealth <= 0)
		{
			Destruccion();

		}
		if (colision.gameObject.CompareTag("Medico"))
		{
			GiveLife(5);
		}
		if (colision.gameObject.CompareTag("Bomba"))
		{
			TakeDamage(10);
		}

	}

	void TakeDamage(int damage)
	{
		currentHealth -= damage;

		healthBar.setHealth(currentHealth);
	}
	void GiveLife(int damage)
	{
		currentHealth += damage;

		healthBar.setHealth(currentHealth);
	}


	public void Destruccion()
	{
		Destroy(this.gameObject);


		//Persistencia.instancia.GuardarDataPersistencia();

	}
}
