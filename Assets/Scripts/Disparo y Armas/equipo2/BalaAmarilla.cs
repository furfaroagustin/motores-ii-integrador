﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaAmarilla : MonoBehaviour
{
    
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player")) 
        {
            Destroy(this.gameObject);
            Destroy(gameObject);

        }
        if (collision.gameObject.CompareTag("Piedra")) 
        {
            Destroy(this.gameObject);
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("Castillo"))
        {
            Destroy(this.gameObject);
            Destroy(gameObject);
        }
    }
}