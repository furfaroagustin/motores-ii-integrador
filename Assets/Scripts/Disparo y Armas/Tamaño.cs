﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tamaño : MonoBehaviour
{
    public float tamañoBase = 1f;

    void Update()
    {
        float animacion = tamañoBase + Mathf.Sin(Time.time * 8f) * tamañoBase / 7f;
        transform.localScale = Vector3.one * animacion;
    }
}
