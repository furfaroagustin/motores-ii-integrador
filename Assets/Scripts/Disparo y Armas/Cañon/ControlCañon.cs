﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCañon : MonoBehaviour
{
    public float rotationSpeed = 1;
    public float blastPower = 5;

    public GameObject Cannonball;
    public Transform shotPoint;
    public int playerNumber;
    public GameObject Explosion;
    private void Update()

    {
        if (GameManager.actualPlayer == playerNumber)
        {
            Movimiento();
        }
        
    }
    void Movimiento()
    {
        float HorizontalRotation = Input.GetAxis("Horizontal");
        float VericalRotation = Input.GetAxis("Vertical");

        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles +
            new Vector3(0, HorizontalRotation * rotationSpeed, VericalRotation * rotationSpeed));

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject CreatedCannonball = Instantiate(Cannonball, shotPoint.position, shotPoint.rotation);
            CreatedCannonball.GetComponent<Rigidbody>().velocity = shotPoint.transform.up * blastPower;

            // Added explosion for added effect
            
            Destroy(Instantiate(Explosion, shotPoint.position, shotPoint.rotation), 2);

            // Shake the screen for added effect
            ScreenShake.ShakeAmount = 5;
        }
    }
}
