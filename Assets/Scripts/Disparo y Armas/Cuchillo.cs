﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cuchillo : MonoBehaviour
{
    Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            animator.SetBool("ActivarCuchillo", true);
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            animator.SetBool("ActivarCuchillo", false);
        }
    }
}
